venv:
	python3 -m venv ~/.virtualenvs/ansible-101
	~/.virtualenvs/ansible-101/bin/pip install --upgrade pip
	~/.virtualenvs/ansible-101/bin/pip install ansible molecule
	echo "You should now source ~/.virtualenvs/ansible-101/bin/activate"


