# ansible-101

## install ansible

Using your OS package manager, and/or a python venv.

See ``Makefile`` ``venv``

## Spawn a VBox using vagrant (Optionnal)

```bash
cd vagrant
vagrant up
vagrant ssh-config >> ../ansible/vagrant-ssh-config
# The VM is nammed vagoo7
```

## launch the playbook

```bash
cd ansible
ansible-playbook playbooks/install_cli.yml
```
